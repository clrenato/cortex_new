# Cortex - Eleições    

#### Distribuição de votos por partidos nas unidades político administrativas

## Índice

* [Sobre o projeto](#markdown-header-sobre-o-projeto)
* [Funcionalidades](#markdown-header-funcionalidades)
* [Como executar o projeto](#markdown-header-como-executar-o-projeto)
    * [Pré-requisitos](#markdown-header-pré-requisitos)
    * [Rodando o projeto](#markdown-header-rodando-o-projeto)
* [Tecnologias](#markdown-header-tecnologias)
* [Autor](#markdown-header-autor)

## Sobre o projeto

![projeto](https://bitbucket.org/clrenato/cortex_new/raw/790dfb71faa053302ff9a8452e76406ac8a88730/src/main/resources/static/img/projeto.png)

Sistema que permite visualizar os totais de votações das eleições de 2014, para cada partido, consolidados por regiões, estados, mesorregiões, microrregiões e municípios.

---

## Funcionalidades

![tela](https://bitbucket.org/clrenato/cortex_new/raw/790dfb71faa053302ff9a8452e76406ac8a88730/src/main/resources/static/img/tela.png)

* Na tela inicial, o usuário seleciona o PARTIDO. 
* Será então carregada abaixo uma lista de opções de REGIÃO. É possível escolher múltiplas opções.
* Será então carregada abaixo uma lista de ESTADOS daquelas regiões selecionadas. 
* O carregamento de listas de opções segue de forma subsequente, exibindo MEORREGIÕES, MICRORREGIÕES e MUNICÍPIOS.
* A cada opção selecionada do lado esquerdo, à diretia da tela serão indicadas no mapa as áreas das seleções mais específicas. Ao se passar o mouse sobre a região preenchida, um tooltip indica nome, estado e total de votos. A opacidade da região varia de acordo com os totais de votos de regiões preenchidas.

---

## Como executar o projeto

### Pré-requisitos

Antes de começar, você precisa ter instalado na sua máquina as seguinte ferramentas:

* MySQL v.5.7 ou posterior.
* JAVA 8; 

### Rodando o projeto  

    # Clone este repositório
    $ git clone https://clrenato@bitbucket.org/clrenato/cortex_new.git
    
    # Acesse a pasta do projeto no terminal/cmd
    $ cd ROOT_PROJECT_FOLDER 
    
    # Importe no MySQL o arquivo "eleicoes.sql". 
    # Será gerado o schema "cortex" com as tabelas candidato, candidato_import, estado, mesorregiao, microrregiao, municipio, partido, regiao e uf 
    
    # Ainda na pasta raiz do projeto, gere o BUILD da aplicação
    $ ./gradlew build
    
    # Execute a aplicação em modo de desenvolvimento
    $ java -jar -Dspring.profiles.active=dev  build/libs/eleicoes-1.0.jar 
    
    # O servidor inciará na porta 8080 - acesse http://localhost:8080 

### Estrutura

Este projeto é divido em duas partes:

1. Backend: pasta src/main/java com código fonte JAVA 
2. Frontend: pasta src/main/resources com os assets (css e js) na pasta static e os htmls na pasta templates

O Frontend precisa que o Backend esteja sendo executado para funcionar.

![estrutura](https://bitbucket.org/clrenato/cortex_new/raw/6f5cce45a8d387b996f35c8a73f4f09f88281e7c/src/main/resources/static/img/estrutura.png)

## Tecnologias

* [JAVA 8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)
* [MySQL 5.7.31](https://dev.mysql.com/downloads/mysql/5.7.html)

### Utilitários

* [IntelliJ Java IDE](https://www.jetbrains.com/pt-br/idea/)
* [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)
* API:
    * [IBGE localidades](https://servicodados.ibge.gov.br/api/docs/localidades?versao=1)
    * [IBGE malhas](https://servicodados.ibge.gov.br/api/docs/malhas?versao=2)
* [jQuery](https://jquery.com/)
* Mapa: [Leaflet](https://leafletjs.com/)

## Autor

Carlos Renato Almeida

Desenvolver Web

[clrenato@gmail.com](clrenato@gmail.com)

[LinkeIn](https://www.linkedin.com/in/carlos-renato-almeida-6185916/)