package br.com.cortex.eleicoes.controller.api;

import br.com.cortex.eleicoes.business.api.PartidoBusiness;
import br.com.cortex.eleicoes.response.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/eleicao/2014/presidente/primeiro-turno/partidos")
public class PartidoController {

    private static final Logger logger = LoggerFactory.getLogger(PartidoController.class);


    @Autowired
    private PartidoBusiness partidoBusiness;



    @GetMapping({"/list"})
    public ResponseEntity<List<PartidoResponse>> getPartidoList() {

        try {
            List<PartidoResponse> partidos = partidoBusiness.getPartidoResponseList();

            if (nonNull(partidos) && !partidos.isEmpty()) {
                return ResponseEntity.ok(partidos);
            }

        } catch (Exception e) {
            logger.error("PartidoController -> getPartidoList: ", e);
        }

        return ResponseEntity.notFound().build();
    }




    @GetMapping({"/{partido}"})
    public ResponseEntity<List<RegiaoResponse>> getPartido(
            @PathVariable(required = true) String partido) {

        try {
            List<RegiaoResponse> regiaoResponseList = partidoBusiness.getRegiaoResponseListByPartido(partido);

            if (nonNull(regiaoResponseList) && !regiaoResponseList.isEmpty()) {
                return ResponseEntity.ok(regiaoResponseList);
            }

        } catch (Exception e) {
            logger.error("PartidoController -> getPartido: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{partido}/{regioes}"})
    public ResponseEntity<List<EstadoResponse>> getPartidoByRegioes(
            @PathVariable(required = true) String partido,
            @PathVariable(required = true) String regioes) {

        try {
            List<EstadoResponse> estadoResponseList = partidoBusiness.getEstadoResponseListByPartido(partido, regioes);

            if (nonNull(estadoResponseList) && !estadoResponseList.isEmpty()) {
                return ResponseEntity.ok(estadoResponseList);
            }

        } catch (Exception e) {
            logger.error("PartidoController -> getPartidoByRegiao: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{partido}/{regioes}/{estados}"})
    public ResponseEntity<List<MesorregiaoResponse>> getPartidoByMesorregioes(
            @PathVariable(required = true) String partido,
            @PathVariable(required = true) String regioes,
            @PathVariable(required = true) String estados) {

        try {
            List<MesorregiaoResponse> mesorregiaoResponseList = partidoBusiness
                    .getMesorregiaoResponseListByPartido(partido, regioes, estados);

            if (nonNull(mesorregiaoResponseList) && !mesorregiaoResponseList.isEmpty()) {
                return ResponseEntity.ok(mesorregiaoResponseList);
            }

        } catch (Exception e) {
            logger.error("PartidoController -> getPartidoByRegiao: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{partido}/{regioes}/{estados}/{mesorregioes}"})
    public ResponseEntity<List<MicrorregiaoResponse>> getPartidoByMicrorregioes(
            @PathVariable(required = true) String partido,
            @PathVariable(required = true) String regioes,
            @PathVariable(required = true) String estados,
            @PathVariable(required = true) String mesorregioes) {

        try {
            List<MicrorregiaoResponse> microrregiaoResponseList = partidoBusiness
                    .getMicrorregiaoResponseListByPartido(partido, regioes, estados, mesorregioes);

            if (nonNull(microrregiaoResponseList) && !microrregiaoResponseList.isEmpty()) {
                return ResponseEntity.ok(microrregiaoResponseList);
            }

        } catch (Exception e) {
            logger.error("PartidoController -> getPartidoByRegiao: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{partido}/{regioes}/{estados}/{mesorregioes}/{microrregioes}"})
    public ResponseEntity<List<MunicipioResponse>> getPartidoByMunicipios(
            @PathVariable(required = true) String partido,
            @PathVariable(required = true) String regioes,
            @PathVariable(required = true) String estados,
            @PathVariable(required = true) String mesorregioes,
            @PathVariable(required = true) String microrregioes) {

        try {
            List<MunicipioResponse> municipioResponseList = partidoBusiness
                    .geMunicipioResponseListByPartido(partido, regioes, estados, mesorregioes, microrregioes);

            if (nonNull(municipioResponseList) && !municipioResponseList.isEmpty()) {
                return ResponseEntity.ok(municipioResponseList);
            }

        } catch (Exception e) {
            logger.error("PartidoController -> getPartidoByMunicipios: ", e);
        }

        return ResponseEntity.notFound().build();
    }



}
