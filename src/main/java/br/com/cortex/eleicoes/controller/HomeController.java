package br.com.cortex.eleicoes.controller;

import br.com.cortex.eleicoes.business.HomeBusiness;
import br.com.cortex.eleicoes.controller.api.PartidoController;
import br.com.cortex.eleicoes.response.HomeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = {"/", "/index"})
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private HomeBusiness business;

    @GetMapping
    public String home(Model model, HttpServletRequest request) {

        HomeResponse response = business.getHomeResponse();

        model.addAttribute("response", response);

        return "index";
    }

}
