package br.com.cortex.eleicoes.controller.api;

import br.com.cortex.eleicoes.business.api.GeoJsonBusiness;
import mil.nga.sf.geojson.FeatureCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/eleicao/2014/presidente/primeiro-turno/geojson")
public class GeoJsonController {

    private static final Logger logger = LoggerFactory.getLogger(GeoJsonController.class);

    @Autowired
    private GeoJsonBusiness geoJsonBusiness;


    @GetMapping({"/{localidadeId}"})
    public ResponseEntity<FeatureCollection> getByRegiaoId(
            @PathVariable(required = true) Integer localidadeId,
            @RequestParam(value = "type", required = true) Integer type) {

        try {
            FeatureCollection featureCollection = geoJsonBusiness.findByLocalidadeId(localidadeId, type);

            if (nonNull(featureCollection)) {
                return ResponseEntity.ok(featureCollection);
            }

        } catch (Exception e) {
            logger.error("GeoJsonController -> getPartido: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    //https://servicodados.ibge.gov.br/api/v2/malhas/3?formato=application/vnd.geo+json


}
