package br.com.cortex.eleicoes.controller.api;

import br.com.cortex.eleicoes.business.api.*;
import br.com.cortex.eleicoes.response.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/eleicao/2014/presidente/primeiro-turno")
public class UnidadesController {

    private static final Logger logger = LoggerFactory.getLogger(UnidadesController.class);

    @Autowired
    private RegiaoBusiness regiaoBusiness;

    @Autowired
    private EstadoBusiness estadoBusiness;

    @Autowired
    private MesorregiaoBusiness mesorregiaoBusiness;

    @Autowired
    private MicrorregiaoBusiness microrregiaoBusiness;

    @Autowired
    private MunicipioBusiness municipioBusiness;


    @GetMapping({"/", ""})
    public ResponseEntity<List<RegiaoResponse>> getRegiaoResponseList() {

        try {
            List<RegiaoResponse> list = regiaoBusiness.getRegiaoResponseList();

            if (nonNull(list)) {
                return ResponseEntity.ok(list);
            }

        } catch (Exception e) {
            logger.error("VotoController -> getRegiaoResponseList: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{regioes}"})
    public ResponseEntity<List<EstadoResponse>> getEstadoResponseList(@PathVariable(required = true) String regioes) {

        try {
            List<EstadoResponse> list = estadoBusiness.getEstadoResponseList(regioes);

            if (nonNull(list)) {
                return ResponseEntity.ok(list);
            }

        } catch (Exception e) {
            logger.error("VotoController -> getEstadoResponseList: ", e);
        }

        return ResponseEntity.notFound().build();
    }




    @GetMapping({"/{regioes}/{estados}"})
    public ResponseEntity<List<MesorregiaoResponse>> getMesorregiaoResponseList(
            @PathVariable(required = true) String regioes,
            @PathVariable(required = true) String estados) {

        try {
            List<MesorregiaoResponse> mesoResponseList = mesorregiaoBusiness.getMesoResponseList(regioes, estados);

            if (nonNull(mesoResponseList)) {
                return ResponseEntity.ok(mesoResponseList);
            }

        } catch (Exception e) {
            logger.error("VotoController -> getMesorregiaoResponseList: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{regioes}/{estados}/{mesorregioes}"})
    public ResponseEntity<List<MicrorregiaoResponse>> getMicrorregiaoResponseList(
            @PathVariable(required = true) String regioes,
            @PathVariable(required = true) String estados,
            @PathVariable(required = true) String mesorregioes) {

        try {

            List<MicrorregiaoResponse> microResponseList = microrregiaoBusiness.getMicroResponseList(regioes, estados, mesorregioes);

            if (nonNull(microResponseList)) {
                return ResponseEntity.ok(microResponseList);
            }


        } catch (Exception e) {
            logger.error("VotoController -> getMicrorregiaoResponseList: ", e);
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping({"/{regioes}/{estados}/{mesorregioes}/{microrregioes}"})
    public ResponseEntity<List<MunicipioResponse>> getMunicipiosResponseList(
            @PathVariable(required = true) String regioes,
            @PathVariable(required = true) String estados,
            @PathVariable(required = true) String mesorregioes,
            @PathVariable(required = true) String microrregioes) {

        try {

            List<MunicipioResponse> municipioResponseList = municipioBusiness.getMunicipioResponseList(
                    regioes, estados, mesorregioes, microrregioes);

            if (nonNull(municipioResponseList)) {
                return ResponseEntity.ok(municipioResponseList);
            }


        } catch (Exception e) {
            logger.error("VotoController -> getMicrorregiaoResponseList: ", e);
        }

        return ResponseEntity.notFound().build();
    }

}
