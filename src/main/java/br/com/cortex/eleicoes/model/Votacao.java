package br.com.cortex.eleicoes.model;

public class Votacao {

    private String candidatoPartido;
    private Integer totalVotos;

    public String getCandidatoPartido() {
        return candidatoPartido;
    }

    public void setCandidatoPartido(String candidatoPartido) {
        this.candidatoPartido = candidatoPartido;
    }

    public Integer getTotalVotos() {
        return totalVotos;
    }

    public void setTotalVotos(Integer totalVotos) {
        this.totalVotos = totalVotos;
    }
}
