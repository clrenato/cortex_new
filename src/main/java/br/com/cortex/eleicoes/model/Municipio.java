package br.com.cortex.eleicoes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class Municipio {

    private Integer id;
    private String nome;
    private Microrregiao microrregiao;

    @JsonIgnore
    private String muCode;
    @JsonIgnore
    private String skuCode;
    @JsonIgnore
    private String ufName;
    @JsonIgnore
    private String uf;
    @JsonIgnore
    private List<CandidatoImport> candidadatos;
    @JsonIgnore
    private Integer estadoId;
    @JsonIgnore
    private Integer microrregiaoId;
    @JsonIgnore
    private String slug;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMuCode() {
        return muCode;
    }

    public void setMuCode(String muCode) {
        this.muCode = muCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getUfName() {
        return ufName;
    }

    public void setUfName(String ufName) {
        this.ufName = ufName;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public List<CandidatoImport> getCandidadatos() {
        return candidadatos;
    }

    public void setCandidadatos(List<CandidatoImport> candidadatos) {
        this.candidadatos = candidadatos;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Integer getMicrorregiaoId() {
        return microrregiaoId;
    }

    public void setMicrorregiaoId(Integer microrregiaoId) {
        this.microrregiaoId = microrregiaoId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Microrregiao getMicrorregiao() {
        return microrregiao;
    }

    public void setMicrorregiao(Microrregiao microrregiao) {
        this.microrregiao = microrregiao;
    }

    @Override
    public String toString() {
        return "Municipio{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", microrregiao=" + microrregiao +
                ", muCode='" + muCode + '\'' +
                ", skuCode='" + skuCode + '\'' +
                ", ufName='" + ufName + '\'' +
                ", uf='" + uf + '\'' +
                ", candidadatos=" + candidadatos +
                ", estadoId=" + estadoId +
                ", microrregiaoId=" + microrregiaoId +
                ", slug='" + slug + '\'' +
                '}';
    }
}
