package br.com.cortex.eleicoes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Microrregiao {

    private Integer id;
    private String nome;
    private Mesorregiao mesorregiao;

    @JsonIgnore
    private Integer mesorregiaoId;
    @JsonIgnore
    private String slug;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getMesorregiaoId() {
        return mesorregiaoId;
    }

    public void setMesorregiaoId(Integer mesorregiaoId) {
        this.mesorregiaoId = mesorregiaoId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Mesorregiao getMesorregiao() {
        return mesorregiao;
    }

    public void setMesorregiao(Mesorregiao mesorregiao) {
        this.mesorregiao = mesorregiao;
    }

    @Override
    public String toString() {
        return "Microrregiao{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", mesorregiaoId=" + mesorregiaoId +
                ", slug='" + slug + '\'' +
                '}';
    }
}
