package br.com.cortex.eleicoes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Mesorregiao {

    private Integer id;
    private String nome;
    private Estado estado;

    @JsonIgnore
    private Integer estadoId;
    @JsonIgnore
    private String slug;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Mesorregiao{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", estadoId=" + estadoId +
                ", slug='" + slug + '\'' +
                '}';
    }
}
