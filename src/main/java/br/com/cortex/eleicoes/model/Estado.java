package br.com.cortex.eleicoes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Estado {

    private Integer id;
    private String sigla;
    private String nome;
    private Regiao regiao;
    private Integer apiId;

    @JsonIgnore
    private Integer regiaoId;
    @JsonIgnore
    private List<CandidatoImport> candidadatos;
    @JsonIgnore
    private String ufCode;
    @JsonIgnore
    private String code;
    @JsonIgnore
    private Integer ufId;
    @JsonIgnore
    private String slug;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUfCode() {
        return ufCode;
    }

    public void setUfCode(String ufCode) {
        this.ufCode = ufCode;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<CandidatoImport> getCandidadatos() {
        return candidadatos;
    }

    public void setCandidadatos(List<CandidatoImport> candidadatos) {
        this.candidadatos = candidadatos;
    }

    public Integer getUfId() {
        return ufId;
    }

    public void setUfId(Integer ufId) {
        this.ufId = ufId;
    }

    public Integer getRegiaoId() {
        return regiaoId;
    }

    public void setRegiaoId(Integer regiaoId) {
        this.regiaoId = regiaoId;
    }

    public Regiao getRegiao() {
        return regiao;
    }

    public void setRegiao(Regiao regiao) {
        this.regiao = regiao;
    }

    public Integer getApiId() {
        return apiId;
    }

    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @Override
    public String toString() {
        return "Estado{" +
                "id=" + id +
                ", sigla='" + sigla + '\'' +
                ", nome='" + nome + '\'' +
                ", regiao=" + regiao +
                ", regiaoId=" + regiaoId +
                ", candidadatos=" + candidadatos +
                ", ufCode='" + ufCode + '\'' +
                ", code='" + code + '\'' +
                ", ufId=" + ufId +
                ", apiId=" + apiId +
                ", slug='" + slug + '\'' +
                '}';
    }
}
