package br.com.cortex.eleicoes.model;

public class CandidatoImport {

    private Integer id;
    private String partido;
    private String nome;
    private Integer votos;
    private Double doubleValue;
    private String letter;
    private Integer municipioId;
    private Integer estadoId;
    private String seoNome;
    private Integer candidatoId;
    private Integer partidoId;
    private Integer mesorregiaoId;
    private Integer microrregiaoId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPartido() {
        return partido;
    }

    public void setPartido(String partido) {
        this.partido = partido;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getVotos() {
        return votos;
    }

    public void setVotos(Integer votos) {
        this.votos = votos;
    }

    public Double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(Double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public Integer getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(Integer municipioId) {
        this.municipioId = municipioId;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public String getSeoNome() {
        return seoNome;
    }

    public void setSeoNome(String seoNome) {
        this.seoNome = seoNome;
    }

    public Integer getCandidatoId() {
        return candidatoId;
    }

    public void setCandidatoId(Integer candidatoId) {
        this.candidatoId = candidatoId;
    }

    public Integer getPartidoId() {
        return partidoId;
    }

    public void setPartidoId(Integer partidoId) {
        this.partidoId = partidoId;
    }

    public Integer getMesorregiaoId() {
        return mesorregiaoId;
    }

    public void setMesorregiaoId(Integer mesorregiaoId) {
        this.mesorregiaoId = mesorregiaoId;
    }

    public Integer getMicrorregiaoId() {
        return microrregiaoId;
    }

    public void setMicrorregiaoId(Integer microrregiaoId) {
        this.microrregiaoId = microrregiaoId;
    }


    @Override
    public String toString() {
        return "CandidatoImport{" +
                "id=" + id +
                ", partido='" + partido + '\'' +
                ", nome='" + nome + '\'' +
                ", votos=" + votos +
                ", doubleValue=" + doubleValue +
                ", letter='" + letter + '\'' +
                ", municipioId=" + municipioId +
                ", estadoId=" + estadoId +
                ", seoNome='" + seoNome + '\'' +
                ", candidatoId=" + candidatoId +
                ", partidoId=" + partidoId +
                ", mesorregiaoId=" + mesorregiaoId +
                ", microrregiaoId=" + microrregiaoId +
                '}';
    }
}
