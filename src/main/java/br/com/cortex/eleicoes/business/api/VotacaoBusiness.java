package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.EstadoDAO;
import br.com.cortex.eleicoes.dao.VotacaoDAO;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Votacao;
import br.com.cortex.eleicoes.response.api.*;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class VotacaoBusiness {

    private static final Logger logger = LoggerFactory.getLogger(VotacaoBusiness.class);

    @Autowired
    private VotacaoDAO votacaoDAO;

    @Autowired
    private EstadoDAO estadoDAO;


    public void setVotacao(List<? extends BaseResponse> baseResponseList) {
        for (BaseResponse br : baseResponseList) {
            setVotacao(br);
        }
    }


    public void setVotacao(BaseResponse baseResponse) {

        List<Votacao> votacoes = new ArrayList<>();

        if (baseResponse instanceof RegiaoResponse) {

            try {
                RegiaoResponse resp = (RegiaoResponse) baseResponse;

                List<Estado> estados = estadoDAO.findByRegiao(resp.getRegiao().getId());

                List<Integer> estadoIds = estados.stream()
                        .map(Estado::getId)
                        .collect(Collectors.toList());

                votacoes = votacaoDAO.findByRegiaoId(estadoIds);

            } catch (Exception e) {
                logger.error("VotacaoBusiness -> setVotacao Regiao: " , e);
            }

        } else if (baseResponse instanceof EstadoResponse) {

            try {
                EstadoResponse resp = (EstadoResponse) baseResponse;

                votacoes = votacaoDAO.findByEstadoId(Arrays.asList(resp.getEstado().getId()));

            } catch (Exception e) {
                logger.error("VotacaoBusiness -> setVotacao Estado: " , e);
            }

        } else if (baseResponse instanceof MesorregiaoResponse) {

            try {
                MesorregiaoResponse resp = (MesorregiaoResponse) baseResponse;

                votacoes = votacaoDAO.findByMesorregiaoId(Arrays.asList(resp.getMesorregiao().getId()));

            } catch (Exception e) {
                logger.error("VotacaoBusiness -> setVotacao Mesorregiao: " , e);
            }

        } else if (baseResponse instanceof MicrorregiaoResponse) {

            try {
                MicrorregiaoResponse resp = (MicrorregiaoResponse) baseResponse;

                votacoes = votacaoDAO.findByMicrorregiaoId(Arrays.asList(resp.getMicrorregiao().getId()));

            } catch (Exception e) {
                logger.error("VotacaoBusiness -> setVotacao Microrregiao: " , e);
            }

        } else if (baseResponse instanceof MunicipioResponse) {

            try {
                MunicipioResponse resp = (MunicipioResponse) baseResponse;

                votacoes = votacaoDAO.findByMunicipioId(Arrays.asList(resp.getMunicipio().getId()));

            } catch (Exception e) {
                logger.error("VotacaoBusiness -> setVotacao Municipio: " , e);
            }

        }


        if (isNull(votacoes) || votacoes.isEmpty()) {
            return;
        }

        List<VotoResponse> votoResponseList = getVotoResponse(votacoes);

        baseResponse.setCandidatos(votoResponseList);
        baseResponse.setTotalVotos(getTotalVotos(votacoes));

    }


    private Integer getTotalVotos(List<Votacao> votacaos) {

        Integer result = 0;

        try {
            for (Votacao v : votacaos) {

                result += v.getTotalVotos();
            }

        } catch (Exception e) {
            logger.error("VotacaoBusiness -> getTotalVotos: " , e);
        }

        return result;

    }


    private List<VotoResponse> getVotoResponse(List<Votacao> votacaos) {

        List<VotoResponse> responseList = new ArrayList<>();

        for (Votacao v : votacaos) {
            responseList.add(getVotoResponse(v));
        }

        return responseList;
    }


    private VotoResponse getVotoResponse(Votacao v) {

        VotoResponse resp = new VotoResponse();

        try {
            //parse candidato-partido format
            String cap = v.getCandidatoPartido().replace("-", " ");

            String[] arr = cap.split(" ");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < arr.length; i++) {

                if (i == arr.length - 1) {
                    break;
                }

                sb.append(WordUtils.capitalize(arr[i]));

                if (i < arr.length - 2) {
                    sb.append(" ");
                }
            }

            resp.setCandidato(sb.toString());

            resp.setPartido(arr[arr.length - 1].toUpperCase());

            resp.setVotos(v.getTotalVotos());

        } catch (Exception e) {
            logger.error("VotacaoBusiness -> getVotoResponse: " , e);
        }

        return resp;

    }


}
