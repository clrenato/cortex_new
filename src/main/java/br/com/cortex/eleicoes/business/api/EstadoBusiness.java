package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.EstadoDAO;
import br.com.cortex.eleicoes.dao.RegiaoDAO;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.response.api.EstadoResponse;
import br.com.cortex.eleicoes.utils.NumberUtils;
import br.com.cortex.eleicoes.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Component
public class EstadoBusiness {

    private static final Logger logger = LoggerFactory.getLogger(EstadoBusiness.class);

    @Autowired
    private RegiaoDAO regiaoDAO;

    @Autowired
    private EstadoDAO estadoDAO;

    @Autowired
    private VotacaoBusiness votacaoBusiness;


    public List<EstadoResponse> getEstadoResponseList(String regioes) {

        try {
            List<EstadoResponse> responseList = new ArrayList<>();

            String[] regioesParam = regioes.split("\\|");

            List<Estado> regiaoEstados = filterEstadosByRegionParam(regioesParam);

            for (Estado estado : regiaoEstados) {

                estado.setRegiao(regiaoDAO.findById(estado.getRegiaoId()));

                EstadoResponse estadoResponse = new EstadoResponse();
                estadoResponse.setEstado(estado);

                responseList.add(estadoResponse);
            }

            votacaoBusiness.setVotacao(responseList);

            return responseList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> getRegiaoResponseList: ", e);
        }

        return null;
    }


    List<Estado> filterEstadosByRegionParam(String[] regioes) {

        List<Estado> allEstados = new ArrayList<>();

        try {
            for (String r : regioes) {

                Regiao regiao = NumberUtils.isNumeric(r) ?
                        regiaoDAO.findById(Integer.valueOf(r)) : regiaoDAO.findByNomeOrSigla(r);

                if (isNull(regiao)) {
                    continue;
                }

                List<Estado> regiaoEstados = estadoDAO.findByRegiao(regiao.getId());

                if (nonNull(regiaoEstados) && !regiaoEstados.isEmpty()) {

                    for (Estado estado : regiaoEstados) {
                        estado.setRegiao(regiaoDAO.findById(estado.getRegiaoId()));
                    }

                    allEstados.addAll(regiaoEstados);
                }
            }

        } catch (Exception e) {
            logger.error("VotoBusiness -> filterRegionsByParam: ", e);
        }

        return allEstados;
    }


    List<Estado> filterEstadosByParam(List<Estado> regiaoEstados, String[] estados) {

        List<Estado> filtered = new ArrayList<>();

        try {
            for (String est : estados) {

                if (NumberUtils.isNumeric(est)) {

                    List<Estado> mEstado = regiaoEstados.stream()
                            .filter(f -> f.getId().equals(Integer.valueOf(est)))
                            .collect(Collectors.toList());

                    filtered.addAll(mEstado);

                } else {

                    List<Estado> mEstado = regiaoEstados.stream()
                            .filter(f -> f.getSlug().equalsIgnoreCase(StringUtils.toSlug(est))
                                    || f.getSigla().equalsIgnoreCase(est))
                            .collect(Collectors.toList());

                    filtered.addAll(mEstado);

                }
            }

            return filtered;

        } catch (Exception e) {
            logger.error("VotoBusiness -> filterEstadosByParam: " , e);
        }

        return null;
    }

}
