package br.com.cortex.eleicoes.business;

import br.com.cortex.eleicoes.business.api.PartidoBusiness;
import br.com.cortex.eleicoes.response.HomeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeBusiness {

    private static final Logger logger = LoggerFactory.getLogger(HomeBusiness.class);

    @Autowired
    private PartidoBusiness partidoBusiness;


    public HomeResponse getHomeResponse() {

        HomeResponse response = new HomeResponse();

        response.setPartidoList(partidoBusiness.findAll());

        return response;
    }

}
