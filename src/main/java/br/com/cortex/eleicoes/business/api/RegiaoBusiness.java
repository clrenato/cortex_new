package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.RegiaoDAO;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.response.api.RegiaoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RegiaoBusiness {

    private static final Logger logger = LoggerFactory.getLogger(RegiaoBusiness.class);

    @Autowired
    private RegiaoDAO regiaoDAO;

    @Autowired
    private VotacaoBusiness votacaoBusiness;


    public List<RegiaoResponse> getRegiaoResponseList() {

        try {
            List<RegiaoResponse> responseList = new ArrayList<>();

            List<Regiao> regiaoList = regiaoDAO.findAll();

            for (Regiao regiao : regiaoList) {

                RegiaoResponse regiaoResponse = new RegiaoResponse();
                regiaoResponse.setRegiao(regiao);

                responseList.add(regiaoResponse);
            }

            votacaoBusiness.setVotacao(responseList);

            return responseList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> getRegiaoResponseList: ", e);
        }

        return null;
    }


}
