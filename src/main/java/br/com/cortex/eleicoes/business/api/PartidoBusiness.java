package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.*;
import br.com.cortex.eleicoes.model.*;
import br.com.cortex.eleicoes.response.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class PartidoBusiness {

    private static final Logger logger = LoggerFactory.getLogger(PartidoBusiness.class);

    @Autowired
    private PartidoDAO partidoDAO;

    @Autowired
    private RegiaoBusiness regiaoBusiness;

    @Autowired
    private EstadoBusiness estadoBusiness;

    @Autowired
    private MesorregiaoBusiness mesorregiaoBusiness;

    @Autowired
    private MicrorregiaoBusiness microrregiaoBusiness;

    @Autowired
    private MunicipioBusiness municipioBusiness;


    public List<Partido> findAll() {

        try {
            return partidoDAO.findAll();

        } catch(Exception e) {
            logger.error("VotoController -> getRegiaoResponseList: ", e);
        }

        return null;

    }


    public List<PartidoResponse> getPartidoResponseList() {

        List<PartidoResponse> responseList = new ArrayList<>();

        try {
            List<Partido> partidos = findAll();

            if (nonNull(partidos) && !partidos.isEmpty()) {

                for (Partido p : partidos) {

                    PartidoResponse resp = new PartidoResponse();
                    resp.setPartido(p);

                    responseList.add(resp);
                }
            }

        } catch(Exception e) {
            logger.error("VotoController -> getRegiaoResponseList: ", e);
        }

        return responseList;
    }


    public List<RegiaoResponse> getRegiaoResponseListByPartido(String partido) {

        List<RegiaoResponse> responseList = new ArrayList<>();

        try {
            responseList = regiaoBusiness.getRegiaoResponseList();

            filterByPartido(responseList, partido);

        } catch (Exception e) {
            logger.error("VotoController -> getRegiaoResponseList: ", e);
        }

        return responseList;
    }



    public List<EstadoResponse> getEstadoResponseListByPartido(String partido, String regioes) {

        List<EstadoResponse> responseList = new ArrayList<>();

        try {
            responseList = estadoBusiness.getEstadoResponseList(regioes);

            filterByPartido(responseList, partido);

        } catch (Exception e) {
            logger.error("VotoController -> getEstadoResponseListByPartido: ", e);
        }

        return responseList;
    }

    public List<MesorregiaoResponse> getMesorregiaoResponseListByPartido(
            String partido, String regioes, String estados) {

        List<MesorregiaoResponse> responseList = new ArrayList<>();

        try {
            responseList = mesorregiaoBusiness.getMesoResponseList(regioes, estados);

            filterByPartido(responseList, partido);

        } catch (Exception e) {
            logger.error("VotoController -> getMesorregiaoResponseListByPartido: ", e);
        }

        return responseList;
    }


    public List<MicrorregiaoResponse> getMicrorregiaoResponseListByPartido(
            String partido, String regioes, String estados, String mesorregioes) {

        List<MicrorregiaoResponse> responseList = new ArrayList<>();

        try {
            responseList = microrregiaoBusiness.getMicroResponseList(regioes, estados, mesorregioes);

            filterByPartido(responseList, partido);

        } catch (Exception e) {
            logger.error("VotoController -> getMicrorregiaoResponseListByPartido: ", e);
        }

        return responseList;
    }

    public List<MunicipioResponse> geMunicipioResponseListByPartido(
            String partido, String regioes, String estados, String mesorregioes, String microrregioes) {

        List<MunicipioResponse> responseList = new ArrayList<>();

        try {
            responseList = municipioBusiness.getMunicipioResponseList(regioes, estados, mesorregioes, microrregioes);

            filterByPartido(responseList, partido);

        } catch (Exception e) {
            logger.error("VotoController -> getMicrorregiaoResponseListByPartido: ", e);
        }

        return responseList;
    }




    private void filterByPartido(List<? extends BaseResponse> baseResponseList, String partido) {

        for (BaseResponse baseResponse : baseResponseList) {
            filterByPartido(baseResponse, partido);
        }
    }


    private void filterByPartido(BaseResponse baseResponse, String partido) {

        try {
            List<VotoResponse> filteredCandidato = baseResponse
                    .getCandidatos()
                    .stream()
                    .filter(f -> f.getPartido().equalsIgnoreCase(partido))
                    .collect(Collectors.toList());

            if (nonNull(filteredCandidato) && !filteredCandidato.isEmpty()) {
                baseResponse.setCandidatos(filteredCandidato);
                baseResponse.setTotalVotos(filteredCandidato.get(0).getVotos());
            }

        } catch (Exception e) {
            logger.error("VotoController -> filterByPartido: ", e);
        }

    }

}
