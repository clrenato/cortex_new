package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.*;
import br.com.cortex.eleicoes.model.*;
import br.com.cortex.eleicoes.response.api.MunicipioResponse;
import br.com.cortex.eleicoes.utils.NumberUtils;
import br.com.cortex.eleicoes.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class MunicipioBusiness {

    private static final Logger logger = LoggerFactory.getLogger(MicrorregiaoBusiness.class);

    @Autowired
    private RegiaoDAO regiaoDAO;

    @Autowired
    private EstadoDAO estadoDAO;

    @Autowired
    private MesorregiaoDAO mesorregiaoDAO;

    @Autowired
    private MicrorregiaoDAO microrregiaoDAO;

    @Autowired
    private MunicipioDAO municipioDAO;

    @Autowired
    private EstadoBusiness estadoBusiness;

    @Autowired
    private MesorregiaoBusiness mesorregiaoBusiness;

    @Autowired
    private MicrorregiaoBusiness microrregiaoBusiness;

    @Autowired
    private VotacaoBusiness votacaoBusiness;



    public List<MunicipioResponse> getMunicipioResponseList(String regioesParam, String estadosParam,
                                                            String mesorregioesParam, String microrregioesParam) {

        try {
            List<MunicipioResponse> responseList = new ArrayList<>();

            String[] regioes  = regioesParam.split("\\|");
            String[] estados = estadosParam.split("\\|");
            String[] mesorregioes = mesorregioesParam.split("\\|");
            String[] microrregioes = microrregioesParam.split("\\|");

            List<Estado> regiaoEstados = estadoBusiness.filterEstadosByRegionParam(regioes);

            if (isNull(regiaoEstados) || regiaoEstados.isEmpty()) {
                return null;
            }

            List<Estado> filteredEstados = estadoBusiness.filterEstadosByParam(regiaoEstados, estados);

            List<Mesorregiao> mesorregiaoList = mesorregiaoBusiness.filterMesoByEstado(filteredEstados);

            if (isNull(mesorregiaoList) || mesorregiaoList.isEmpty()) {
                return null;
            }

            List<Microrregiao> microList = microrregiaoBusiness.filterMicroByMesorregiao(mesorregiaoList, mesorregioes);

            if (isNull(microList) || microList.isEmpty()) {
                return null;
            }

            List<Municipio> municipioList = filterMunicipioByMicrorregiao(microList, microrregioes);

            if (isNull(municipioList) || municipioList.isEmpty()) {
                return null;
            }

            for (Municipio municipio : municipioList) {

                Microrregiao nMicro = microrregiaoDAO.findById(municipio.getMicrorregiaoId());
                Mesorregiao nMeso = mesorregiaoDAO.findById(nMicro.getMesorregiaoId());
                Estado nEstado = estadoDAO.findById(nMeso.getEstadoId());
                Regiao nRegiao = regiaoDAO.findById(nEstado.getRegiaoId());

                nEstado.setRegiao(nRegiao);
                nMeso.setEstado(nEstado);
                nMicro.setMesorregiao(nMeso);
                municipio.setMicrorregiao(nMicro);

                MunicipioResponse resp = new MunicipioResponse();
                resp.setMunicipio(municipio);

                responseList.add(resp);
            }

            votacaoBusiness.setVotacao(responseList);

            return responseList;


        } catch (Exception e) {
            logger.error("VotoBusiness -> getMunicipioResponseList: ", e);
        }

        return null;
    }


    List<Municipio> filterMunicipioByMicrorregiao(List<Microrregiao> microList, String[] microrregioesParam) {

        try {
            List<Microrregiao> filtered = new ArrayList<>();

            for (String mr : microrregioesParam) {

                if (NumberUtils.isNumeric(mr)) {

                    List<Microrregiao> mFilter = microList.stream()
                            .filter(f -> f.getId().equals(Integer.valueOf(mr)))
                            .collect(Collectors.toList());

                    filtered.addAll(mFilter);

                } else {

                    List<Microrregiao> mFilter = microList.stream()
                            .filter(f -> f.getSlug().equalsIgnoreCase(StringUtils.toSlug(mr)))
                            .collect(Collectors.toList());

                    filtered.addAll(mFilter);

                }

                if (filtered.isEmpty()) {
                    continue;
                }

            }

            if (filtered.isEmpty()) {
                return null;
            }

            List<Integer> microIds = filtered.stream()
                    .map(Microrregiao::getId)
                    .collect(Collectors.toList());

            List<Municipio> municipioList = municipioDAO.findByMicrorregiaoId(microIds);

            return municipioList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> filterMunicipioByMicrorregiao: ", e);
        }

        return null;

    }


}
