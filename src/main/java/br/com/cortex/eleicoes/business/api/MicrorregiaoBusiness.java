package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.EstadoDAO;
import br.com.cortex.eleicoes.dao.MesorregiaoDAO;
import br.com.cortex.eleicoes.dao.MicrorregiaoDAO;
import br.com.cortex.eleicoes.dao.RegiaoDAO;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Mesorregiao;
import br.com.cortex.eleicoes.model.Microrregiao;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.response.api.MicrorregiaoResponse;
import br.com.cortex.eleicoes.utils.NumberUtils;
import br.com.cortex.eleicoes.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class MicrorregiaoBusiness {

    private static final Logger logger = LoggerFactory.getLogger(MicrorregiaoBusiness.class);

    @Autowired
    private RegiaoDAO regiaoDAO;

    @Autowired
    private EstadoDAO estadoDAO;

    @Autowired
    private MesorregiaoDAO mesorregiaoDAO;

    @Autowired
    private MicrorregiaoDAO microrregiaoDAO;

    @Autowired
    private EstadoBusiness estadoBusiness;

    @Autowired
    private MesorregiaoBusiness mesorregiaoBusiness;

    @Autowired
    private VotacaoBusiness votacaoBusiness;



    public List<MicrorregiaoResponse> getMicroResponseList(String regioesParam, String estadosParam, String mesorregioesParam) {

        try {
            List<MicrorregiaoResponse> responseList = new ArrayList<>();

            String[] regioes  = regioesParam.split("\\|");
            String[] estados = estadosParam.split("\\|");
            String[] mesorregioes = mesorregioesParam.split("\\|");

            List<Estado> regiaoEstados = estadoBusiness.filterEstadosByRegionParam(regioes);

            if (isNull(regiaoEstados) || regiaoEstados.isEmpty()) {
                return null;
            }

            List<Estado> filteredEstados = estadoBusiness.filterEstadosByParam(regiaoEstados, estados);

            List<Mesorregiao> mesorregiaoList = mesorregiaoBusiness.filterMesoByEstado(filteredEstados);

            if (isNull(mesorregiaoList) || mesorregiaoList.isEmpty()) {
                return null;
            }

            List<Microrregiao> microList = filterMicroByMesorregiao(mesorregiaoList, mesorregioes);

            if (isNull(microList) || microList.isEmpty()) {
                return null;
            }

            for (Microrregiao microrregiao : microList) {

                //nested objects
                Mesorregiao nMeso = mesorregiaoDAO.findById(microrregiao.getMesorregiaoId());
                Estado nEstado = estadoDAO.findById(nMeso.getEstadoId());
                Regiao nRegiao = regiaoDAO.findById(nEstado.getRegiaoId());

                nEstado.setRegiao(nRegiao);
                nMeso.setEstado(nEstado);

                microrregiao.setMesorregiao(nMeso);

                MicrorregiaoResponse resp = new MicrorregiaoResponse();
                resp.setMicrorregiao(microrregiao);

                responseList.add(resp);
            }

            votacaoBusiness.setVotacao(responseList);

            return responseList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> getMicroResponseList: ", e);
        }

        return null;
    }


    List<Microrregiao> filterMicroByMesorregiao(List<Mesorregiao> mesorregiaoList, String[] mesosParam) {

        try {
            List<Mesorregiao> filtered = new ArrayList<>();

            for (String m : mesosParam) {

                if (NumberUtils.isNumeric(m))  {

                    List<Mesorregiao> mFilter = mesorregiaoList.stream()
                            .filter(f -> f.getId().equals(Integer.valueOf(m)))
                            .collect(Collectors.toList());

                    filtered.addAll(mFilter);

                } else {

                    List<Mesorregiao> mFilter = mesorregiaoList.stream()
                            .filter(f -> f.getSlug().equalsIgnoreCase(StringUtils.toSlug(m)))
                            .collect(Collectors.toList());

                    filtered.addAll(mFilter);
                }

            }

            if (filtered.isEmpty()) {
                return null;
            }

            List<Integer> mesoIds = filtered.stream()
                    .map(Mesorregiao::getId)
                    .collect(Collectors.toList());

            List<Microrregiao> microList = microrregiaoDAO.findByMesorregiaoId(mesoIds);

            if (isNull(microList) || microList.isEmpty()) {
                return null;
            }

            return microList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> filterMicroByMesorregiao: ", e);
        }

        return null;
    }


}
