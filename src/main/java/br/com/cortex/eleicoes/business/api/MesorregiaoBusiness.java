package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.EstadoDAO;
import br.com.cortex.eleicoes.dao.MesorregiaoDAO;
import br.com.cortex.eleicoes.dao.RegiaoDAO;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Mesorregiao;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.response.api.MesorregiaoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class MesorregiaoBusiness {

    private static final Logger logger = LoggerFactory.getLogger(MesorregiaoBusiness.class);

    @Autowired
    private RegiaoDAO regiaoDAO;

    @Autowired
    private EstadoDAO estadoDAO;

    @Autowired
    private EstadoBusiness estadoBusiness;

    @Autowired
    private MesorregiaoDAO mesorregiaoDAO;

    @Autowired
    private VotacaoBusiness votacaoBusiness;


    public List<MesorregiaoResponse> getMesoResponseList(String regioesParam, String estadoParams) {

        List<MesorregiaoResponse> responseList = new ArrayList<>();

        try {
            String[] regioes  = regioesParam.split("\\|");
            String[] estados = estadoParams.split("\\|");

            List<Estado> regiaoEstados = estadoBusiness.filterEstadosByRegionParam(regioes);

            if (isNull(regiaoEstados) || regiaoEstados.isEmpty()) {
                return null;
            }

            List<Estado> filteredLst = estadoBusiness.filterEstadosByParam(regiaoEstados, estados);

            List<Mesorregiao> mesorregiaoList = filterMesoByEstado(filteredLst);

            if (isNull(mesorregiaoList) || mesorregiaoList.isEmpty()) {
                return null;
            }

            for (Mesorregiao meso : mesorregiaoList) {

                //nested
                Estado nEstado = estadoDAO.findById(meso.getEstadoId());
                Regiao nRegiao = regiaoDAO.findById(nEstado.getRegiaoId());

                nEstado.setRegiao(nRegiao);

                meso.setEstado(nEstado);

                MesorregiaoResponse resp = new MesorregiaoResponse();
                resp.setMesorregiao(meso);

                responseList.add(resp);
            }

            votacaoBusiness.setVotacao(responseList);

            return responseList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> getMesoResponseList: ", e);
        }

        return null;
    }


    List<Mesorregiao> filterMesoByEstado(List<Estado> estadoList) {

        try {
            List<Integer> estadoIds = estadoList.stream()
                    .map(Estado::getId)
                    .collect(Collectors.toList());

            List<Mesorregiao> mesoList = mesorregiaoDAO.findByEstadoId(estadoIds);

            if (isNull(mesoList) || mesoList.isEmpty()) {
                return null;
            }

            return mesoList;

        } catch (Exception e) {
            logger.error("VotoBusiness -> filterMesoByEstado: ", e);
        }

        return null;
    }


}
