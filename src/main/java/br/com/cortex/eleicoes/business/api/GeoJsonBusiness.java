package br.com.cortex.eleicoes.business.api;

import br.com.cortex.eleicoes.dao.EstadoDAO;
import br.com.cortex.eleicoes.dao.RegiaoDAO;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.service.GeoJsonService;
import mil.nga.sf.geojson.FeatureCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class GeoJsonBusiness {

    private static final Logger logger = LoggerFactory.getLogger(GeoJsonBusiness.class);

    @Value("${spring.cache.geojson}")
    private String geoJsonCache;

    @Autowired
    private GeoJsonService geoJsonService;

    @Autowired
    private RegiaoDAO regiaoDAO;

    @Autowired
    private EstadoDAO estadoDAO;


    @Cacheable(value = "geojson-cache", key = "#id.toString().concat('-').concat(#type.toString())")
    public FeatureCollection findByLocalidadeId(Integer id, Integer type) {

        try {
            if (type.equals(0)) { //regiaso
                Regiao regiao = regiaoDAO.findById(id);
                id = regiao.getApiId();

            } else if (type.equals(1)) { //estado
                Estado estado = estadoDAO.findById(id);
                id = estado.getApiId();
            }

            FeatureCollection featureCollection = geoJsonService.findByLocalidadeId(id);

            return featureCollection;

        } catch (Exception e) {
            logger.error("GeoJsonBusiness -> findByRegiaoId: ", e);
        }

        return null;
    }


}
