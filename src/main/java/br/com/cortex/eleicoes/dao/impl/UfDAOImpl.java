package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.UfDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Uf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class UfDAOImpl implements UfDAO {

    private static String FIND_BY_SIGLA = "SELECT id, sigla FROM uf WHERE sigla = ?";

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public Uf findBySigla(String sigla) throws DAOException {
        return jdbcTemplateBean.query(FIND_BY_SIGLA, new Object[] {sigla}, new BeanPropertyRowMapper<>(Uf.class)).get(0);
    }
}
