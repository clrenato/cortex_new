package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.CandidatoImport;
import br.com.cortex.eleicoes.model.Municipio;

import java.util.List;

public interface CandidatoImportDAO {

    int save(CandidatoImport candidatoImport) throws DAOException;

    List<CandidatoImport> findByName(String nome) throws DAOException;

    List<CandidatoImport> findByEstadoId(List<Integer> estadoIds) throws DAOException;

    List<CandidatoImport> findByPartido(String partido) throws DAOException;

    List<CandidatoImport> findByMunicipioId(Integer municipioId) throws DAOException;

    void update(CandidatoImport candidatoImport) throws DAOException;

    void updateMicroAndMesoIds(CandidatoImport candidatoImport) throws DAOException;

}
