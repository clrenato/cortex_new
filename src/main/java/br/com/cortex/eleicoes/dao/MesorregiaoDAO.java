package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Mesorregiao;

import java.util.List;

public interface MesorregiaoDAO {

    int save(Mesorregiao mesorregiao) throws DAOException;

    List<Mesorregiao> findAll() throws DAOException;

    Mesorregiao findById(Integer id) throws DAOException;

    List<Mesorregiao> findByEstadoId(List<Integer> estadoIds) throws DAOException;

    void updateSlug(Mesorregiao mesorregiao) throws DAOException;
}
