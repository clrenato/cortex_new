package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.MesorregiaoDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Mesorregiao;
import br.com.cortex.eleicoes.model.Microrregiao;
import br.com.cortex.eleicoes.model.Regiao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class MesorregiaoDAOImpl implements MesorregiaoDAO {

    private static String INSERT_STATEMENT = "INSERT INTO mesorregiao(id, nome, estado_id) VALUES(?,?,?)";

    private static String SELECT_ALL = "SELECT id, nome, estado_id, slug FROM mesorregiao";

    private static String SELECT_BY_ID = SELECT_ALL + " WHERE id = ? ";

    private static String SELECT_BY_ESTADO_ID = SELECT_ALL + " WHERE estado_id IN(%s) ";

    private static String UPDATE_SLUG = "UPDATE mesorregiao SET slug = ? WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public void updateSlug(Mesorregiao mesorregiao) throws DAOException {
        jdbcTemplateBean.update(UPDATE_SLUG, mesorregiao.getSlug(), mesorregiao.getId());
    }

    @Override
    public int save(Mesorregiao mesorregiao) throws DAOException {
        return jdbcTemplateBean.update(INSERT_STATEMENT, mesorregiao.getId(), mesorregiao.getNome(), mesorregiao.getEstadoId());
    }

    @Override
    public List<Mesorregiao> findAll() throws DAOException {
        return jdbcTemplateBean.query(SELECT_ALL, new BeanPropertyRowMapper<>(Mesorregiao.class));
    }

    @Override
    public Mesorregiao findById(Integer id) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_ID, new Object[] {id}, new BeanPropertyRowMapper<>(Mesorregiao.class)).get(0);
    }

    @Override
    public List<Mesorregiao> findByEstadoId(List<Integer> estadoIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(estadoIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_ESTADO_ID, inSql),
                estadoIds.toArray(),
                new BeanPropertyRowMapper<>(Mesorregiao.class));
    }


}
