package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Votacao;

import java.util.List;

public interface VotacaoDAO {

    List<Votacao> findByRegiaoId(List<Integer> regioesIds) throws DAOException;

    List<Votacao> findByEstadoId(List<Integer> estadosId) throws DAOException;

    List<Votacao> findByMesorregiaoId(List<Integer> mesorregioesIds) throws DAOException;

    List<Votacao> findByMicrorregiaoId(List<Integer> microrregioesIds) throws DAOException;

    List<Votacao> findByMunicipioId(List<Integer> municipioIds) throws DAOException;

}
