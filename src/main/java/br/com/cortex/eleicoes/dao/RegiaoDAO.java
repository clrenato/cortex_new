package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Regiao;

import java.util.List;

public interface RegiaoDAO {

    List<Regiao> findAll() throws DAOException;

    Regiao findBySigla(String sigla) throws DAOException;

    Regiao findByNomeOrSigla(String sigla) throws DAOException;

    Regiao findById(Integer id) throws DAOException;

    void updateSlug(Regiao regiao) throws DAOException;

}
