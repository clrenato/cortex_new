package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Partido;

import java.util.List;

public interface PartidoDAO {

    List<Partido> findAll() throws DAOException;

    Partido findByName(String nome) throws DAOException;

}
