package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.MunicipioDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class MunicipioDAOImpl implements MunicipioDAO {

    private static String INSERT_STATEMENT = "INSERT INTO municipio(id, nome, estado_id) VALUES(?,?,?)";

    private static String UPDATE_STATEMENT = "UPDATE municipio SET nome = ?, estado_id = ?, microrregiao_id = ? WHERE id = ?";

    private static String SELECT_ALL = "SELECT id, nome, estado_id, microrregiao_id, slug FROM municipio";

    private static String SELECT_BY_ID = SELECT_ALL + " WHERE id = ?";

    private static String SELECT_BY_NOME = SELECT_ALL + " WHERE slug = ?";

    private static String SELECT_BY_MICRORREGIAO_ID = SELECT_ALL + " WHERE microrregiao_id IN(%s) ";

    private static String UPDATE_SLUG_STATEMENT = "UPDATE municipio SET slug = ? WHERE id = ?";


    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public void updateSlug(Municipio municipio) throws DAOException {
        jdbcTemplateBean.update(UPDATE_SLUG_STATEMENT, municipio.getSlug(), municipio.getId());
    }

    @Override
    public int save(Municipio municipio) throws DAOException {

        jdbcTemplateBean.update(INSERT_STATEMENT, municipio.getId(), municipio.getNome(), municipio.getEstadoId());

        return municipio.getId();
    }

    @Override
    public void update(Municipio municipio) throws DAOException {
        jdbcTemplateBean.update(UPDATE_STATEMENT, municipio.getNome(),
                municipio.getEstadoId(), municipio.getMicrorregiaoId(), municipio.getId());
    }

    @Override
    public List<Municipio> findAll() throws DAOException {
        return jdbcTemplateBean.query(SELECT_ALL, new BeanPropertyRowMapper<>(Municipio.class));
    }


    @Override
    public Municipio findById(Integer id) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_ID, new Object[] {id}, new BeanPropertyRowMapper<>(Municipio.class)).get(0);
    }

    @Override
    public Municipio findByNome(String nome) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_NOME, new Object[] {nome}, new BeanPropertyRowMapper<>(Municipio.class)).get(0);
    }

    @Override
    public List<Municipio> findByMicrorregiaoId(List<Integer> microIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(microIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_MICRORREGIAO_ID, inSql),
                microIds.toArray(),
                new BeanPropertyRowMapper<>(Municipio.class));
    }


}
