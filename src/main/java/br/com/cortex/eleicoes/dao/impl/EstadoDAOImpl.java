package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.EstadoDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Regiao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EstadoDAOImpl implements EstadoDAO {

    private static String INSERT_STATEMENT = "INSERT INTO estado(nome, sigla, uf_id) VALUES(?,?,?)";

    private static String UPDATE_STATEMENT = "UPDATE estado SET nome = ?, sigla = ?, uf_id = ?, regiao_id = ?, api_id = ? WHERE id = ?";

    private static String SELECT_ALL = "SELECT id, nome, sigla, uf_id, regiao_id, api_id, slug FROM estado";

    private static String SELECT_BY_ID = SELECT_ALL + " WHERE id = ?";

    private static String SELECT_BY_SIGLA = SELECT_ALL + " WHERE sigla = ?";

    private static String SELECT_BY_REGIAO_ID = SELECT_ALL + " WHERE regiao_id = ?";

    private static String UPDATE_SLUG = "UPDATE estado SET slug = ? WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public void updateSlug(Estado estado) throws DAOException {
        jdbcTemplateBean.update(UPDATE_SLUG, estado.getSlug(), estado.getId());
    }

    @Override
    public int save(Estado estado) throws DAOException {
        return jdbcTemplateBean.update(INSERT_STATEMENT, estado.getNome(), estado.getSigla(), estado.getUfId());
    }

    @Override
    public int update(Estado estado) throws DAOException {
        return jdbcTemplateBean.update(UPDATE_STATEMENT, estado.getNome(), estado.getSigla(), estado.getUfId(),
                estado.getRegiaoId(), estado.getApiId(), estado.getId());
    }

    @Override
    public List<Estado> findAll() throws DAOException {
        return jdbcTemplateBean.query(SELECT_ALL, new BeanPropertyRowMapper<>(Estado.class));
    }

    @Override
    public Estado findById(Integer id) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_ID, new Object[] {id}, new BeanPropertyRowMapper<>(Estado.class)).get(0);
    }

    @Override
    public Estado findBySigla(String sigla) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_SIGLA, new Object[] {sigla}, new BeanPropertyRowMapper<>(Estado.class)).get(0);
    }

    @Override
    public List<Estado> findByRegiao(Integer regiaoId) {
        return jdbcTemplateBean.query(SELECT_BY_REGIAO_ID, new Object[] {regiaoId}, new BeanPropertyRowMapper<>(Estado.class));
    }

}
