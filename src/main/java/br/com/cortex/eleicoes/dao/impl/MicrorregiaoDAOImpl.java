package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.MicrorregiaoDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Estado;
import br.com.cortex.eleicoes.model.Mesorregiao;
import br.com.cortex.eleicoes.model.Microrregiao;
import br.com.cortex.eleicoes.model.Regiao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class MicrorregiaoDAOImpl implements MicrorregiaoDAO {

    private static String INSERT_STATEMENT = "INSERT INTO microrregiao(id, nome, mesorregiao_id) VALUES(?,?,?)";

    private static String SELECT_ALL = "SELECT id, nome, mesorregiao_id, slug FROM microrregiao";

    private static String SELECT_BY_ID = SELECT_ALL + " WHERE id = ? ";

    private static String SELECT_BY_MESORREGIAO_ID = SELECT_ALL + " WHERE mesorregiao_id IN(%s) ";

    private static String UPDATE_SLUG = "UPDATE microrregiao SET slug = ? WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public void updateSlug(Microrregiao microrregiao) throws DAOException {
        jdbcTemplateBean.update(UPDATE_SLUG, microrregiao.getSlug(), microrregiao.getId());
    }

    @Override
    public int save(Microrregiao microrregiao) throws DAOException {
        return jdbcTemplateBean.update(INSERT_STATEMENT, microrregiao.getId(),
                microrregiao.getNome(), microrregiao.getMesorregiaoId());
    }

    @Override
    public List<Microrregiao> findAll() throws DAOException {
        return jdbcTemplateBean.query(SELECT_ALL, new BeanPropertyRowMapper<>(Microrregiao.class));
    }

    @Override
    public Microrregiao findById(Integer id) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_ID, new Object[] {id}, new BeanPropertyRowMapper<>(Microrregiao.class)).get(0);
    }

    @Override
    public List<Microrregiao> findByMesorregiaoId(List<Integer> mesoIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(mesoIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_MESORREGIAO_ID, inSql),
                mesoIds.toArray(),
                new BeanPropertyRowMapper<>(Microrregiao.class));
    }

}
