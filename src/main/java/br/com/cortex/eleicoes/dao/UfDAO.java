package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Uf;

public interface UfDAO {

    Uf findBySigla(String sigla) throws DAOException;

}
