package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.CandidatoImportDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.CandidatoImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CandidatoImportDAOImpl implements CandidatoImportDAO {

    private static String INSERT_STATEMENT = "INSERT INTO candidato_import(nome, partido, votos, estado_id, municipio_id, slug) VALUES(?,?,?,?,?,?)";

    private static String SELECT_ALL = "SELECT id, nome, partido, votos, estado_id, municipio_id, slug, candidato_id, partido_id, mesorregiao_id, microrregiao_id FROM candidato_import";

    private static String SELECT_BY_NAME = SELECT_ALL + " WHERE nome = ?";

    private static String SELECT_BY_ESTADO = SELECT_ALL + " WHERE estado_id IN(%s)";

    private static String SELECT_BY_PARTIDO = SELECT_ALL + " WHERE partido = ?";

    private static String SELECT_BY_MUNICIPIO_ID = SELECT_ALL + " WHERE municipio_id = ?";

    private static String UPDATE_STATEMENT = "UPDATE candidato_import SET nome = ?, partido = ?, votos = ?, " +
            "estado_id = ?, municipio_id = ?, seo_nome = ?, candidato_id = ?, partido_id = ? WHERE id = ?";

    private static String UPDATE_MICRO_MESO_IDS = "UPDATE candidato_import SET microrregiao_id = ?, mesorregiao_id = ? WHERE id = ?";


    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public int save(CandidatoImport candidatoImport) throws DAOException {
        return jdbcTemplateBean.update(INSERT_STATEMENT, candidatoImport.getNome(), candidatoImport.getPartido(),
                candidatoImport.getVotos(), candidatoImport.getEstadoId(), candidatoImport.getMunicipioId(), candidatoImport.getSeoNome());
    }

    @Override
    public List<CandidatoImport> findByName(String nome) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_NAME, new Object[] {nome}, new BeanPropertyRowMapper<>(CandidatoImport.class));
    }

    @Override
    public List<CandidatoImport> findByEstadoId(List<Integer> estadoIds) throws DAOException {
        return null;
    }

    @Override
    public List<CandidatoImport> findByPartido(String partido) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_PARTIDO, new Object[] {partido}, new BeanPropertyRowMapper<>(CandidatoImport.class));
    }

    @Override
    public List<CandidatoImport> findByMunicipioId(Integer municipioId) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_MUNICIPIO_ID,
                new Object[] {municipioId}, new BeanPropertyRowMapper<>(CandidatoImport.class));
    }

    @Override
    public void update(CandidatoImport ci) throws DAOException {
        jdbcTemplateBean.update(UPDATE_STATEMENT, ci.getNome(), ci.getPartido(), ci.getVotos(),
                ci.getEstadoId(), ci.getMunicipioId(), ci.getSeoNome(), ci.getCandidatoId(), ci.getPartidoId(), ci.getId());
    }

    @Override
    public void updateMicroAndMesoIds(CandidatoImport ci) throws DAOException {
        jdbcTemplateBean.update(UPDATE_MICRO_MESO_IDS, ci.getMicrorregiaoId(), ci.getMesorregiaoId(), ci.getId());
    }


}
