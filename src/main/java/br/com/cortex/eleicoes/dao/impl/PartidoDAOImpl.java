package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.PartidoDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Candidato;
import br.com.cortex.eleicoes.model.Partido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PartidoDAOImpl implements PartidoDAO {

    private static String SELECT_ALL = "SELECT id, nome FROM partido ORDER BY nome";

    private static String SELECT_BY_NAME = SELECT_ALL + " WHERE nome = ?";

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public List<Partido> findAll() throws DAOException {
        return jdbcTemplateBean.query(SELECT_ALL, new BeanPropertyRowMapper<>(Partido.class));
    }

    @Override
    public Partido findByName(String nome) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_NAME, new Object[] {nome}, new BeanPropertyRowMapper<>(Partido.class)).get(0);
    }
}
