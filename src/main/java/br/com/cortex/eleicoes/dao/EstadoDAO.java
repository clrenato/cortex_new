package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Estado;

import java.util.List;


public interface EstadoDAO {

    int save(Estado estado) throws DAOException;

    int update(Estado estado) throws DAOException;

    List<Estado> findAll() throws DAOException;

    Estado findById(Integer id) throws DAOException;

    Estado findBySigla(String sigla) throws DAOException;

    List<Estado> findByRegiao(Integer regiaoId);

    void updateSlug(Estado estado) throws DAOException;
}
