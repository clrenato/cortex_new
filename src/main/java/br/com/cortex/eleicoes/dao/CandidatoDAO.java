package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Candidato;

import java.util.List;

public interface CandidatoDAO {

    List<Candidato> findAll() throws DAOException;

    Candidato findByName(String nome) throws DAOException;
}
