package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Municipio;
import br.com.cortex.eleicoes.model.Regiao;

import java.util.List;

public interface MunicipioDAO {

    int save(Municipio municipio) throws DAOException;

    void update(Municipio municipio) throws DAOException;

    List<Municipio> findAll() throws DAOException;

    Municipio findById(Integer id) throws DAOException;

    Municipio findByNome(String nome) throws DAOException;

    void updateSlug(Municipio municipio) throws DAOException;

    List<Municipio> findByMicrorregiaoId(List<Integer> microIds) throws DAOException;
}
