package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.VotacaoDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Mesorregiao;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.model.Votacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class VotacaoDAOImpl implements VotacaoDAO {

    private static String SELECT_ALL = "SELECT slug AS 'candidatoPartido', SUM(votos) AS 'totalVotos' FROM candidato_import ";

    private static String GROUP_BY_SLUG = " GROUP BY slug ";

    private static String SELECT_BY_REGIAO_ID = SELECT_ALL + " WHERE estado_id IN(%s) " + GROUP_BY_SLUG;

    private static String SELECT_BY_ESTADO_ID = SELECT_BY_REGIAO_ID;

    private static String SELECT_BY_MESORREGIAO_ID = SELECT_ALL + " WHERE mesorregiao_id IN(%s) " + GROUP_BY_SLUG;

    private static String SELECT_BY_MICRORREGIAO_ID = SELECT_ALL + " WHERE microrregiao_id IN(%s) " + GROUP_BY_SLUG;

    private static String SELECT_BY_MUNICIPIO_ID = SELECT_ALL + " WHERE municipio_id IN(%s) " + GROUP_BY_SLUG;


    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public List<Votacao> findByRegiaoId(List<Integer> regioesIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(regioesIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_REGIAO_ID, inSql),
                regioesIds.toArray(),
                new BeanPropertyRowMapper<>(Votacao.class));

    }

    @Override
    public List<Votacao> findByEstadoId(List<Integer> estadosIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(estadosIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_ESTADO_ID, inSql),
                estadosIds.toArray(),
                new BeanPropertyRowMapper<>(Votacao.class));

    }

    @Override
    public List<Votacao> findByMesorregiaoId(List<Integer> mesorregioesIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(mesorregioesIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_MESORREGIAO_ID, inSql),
                mesorregioesIds.toArray(),
                new BeanPropertyRowMapper<>(Votacao.class));
    }

    @Override
    public List<Votacao> findByMicrorregiaoId(List<Integer> microrregioesIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(microrregioesIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_MICRORREGIAO_ID, inSql),
                microrregioesIds.toArray(),
                new BeanPropertyRowMapper<>(Votacao.class));
    }

    @Override
    public List<Votacao> findByMunicipioId(List<Integer> municipioIds) throws DAOException {

        String inSql = String.join(",", Collections.nCopies(municipioIds.size(), "?"));

        return jdbcTemplateBean.query(
                String.format(SELECT_BY_MUNICIPIO_ID, inSql),
                municipioIds.toArray(),
                new BeanPropertyRowMapper<>(Votacao.class));
    }
}
