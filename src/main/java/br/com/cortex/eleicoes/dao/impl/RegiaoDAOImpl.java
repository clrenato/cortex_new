package br.com.cortex.eleicoes.dao.impl;

import br.com.cortex.eleicoes.dao.RegiaoDAO;
import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Partido;
import br.com.cortex.eleicoes.model.Regiao;
import br.com.cortex.eleicoes.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RegiaoDAOImpl implements RegiaoDAO {

    private static String SELECT_ALL = "SELECT id, nome, sigla, slug, api_id FROM regiao ";

    private static String SELECT_BY_SIGLA = SELECT_ALL + " WHERE sigla = ?";

    private static String SELECT_BY_ID = SELECT_ALL + " WHERE id = ?";

    private static String SELECT_BY_NOME_OR_SIGLA = SELECT_ALL + " WHERE slug = ? OR sigla = ?";

    private static String UPDATE_SLUG = "UPDATE regiao SET slug = ? WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Override
    public List<Regiao> findAll() throws DAOException {
        return jdbcTemplateBean.query(SELECT_ALL, new BeanPropertyRowMapper<>(Regiao.class));
    }

    @Override
    public Regiao findById(Integer id) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_ID, new Object[] {id}, new BeanPropertyRowMapper<>(Regiao.class)).get(0);
    }

    @Override
    public Regiao findByNomeOrSigla(String text) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_NOME_OR_SIGLA,
                new Object[] {StringUtils.toSlug(text), text.toUpperCase()},
                new BeanPropertyRowMapper<>(Regiao.class)).get(0);
    }

    @Override
    public Regiao findBySigla(String sigla) throws DAOException {
        return jdbcTemplateBean.query(SELECT_BY_SIGLA, new Object[] {sigla.toUpperCase()}, new BeanPropertyRowMapper<>(Regiao.class)).get(0);
    }

    @Override
    public void updateSlug(Regiao regiao) throws DAOException {
        jdbcTemplateBean.update(UPDATE_SLUG, regiao.getSlug(), regiao.getId());
    }




}
