package br.com.cortex.eleicoes.dao;

import br.com.cortex.eleicoes.exception.DAOException;
import br.com.cortex.eleicoes.model.Microrregiao;

import java.util.List;

public interface MicrorregiaoDAO {

    int save(Microrregiao microrregiao) throws DAOException;

    List<Microrregiao> findAll() throws DAOException;

    Microrregiao findById(Integer id) throws DAOException;

    List<Microrregiao> findByMesorregiaoId(List<Integer> mesoIds) throws DAOException;

    void updateSlug(Microrregiao microrregiao) throws DAOException;

}
