package br.com.cortex.eleicoes.service.impl;

import br.com.cortex.eleicoes.controller.api.PartidoController;
import br.com.cortex.eleicoes.exception.ServiceException;
import br.com.cortex.eleicoes.model.Municipio;
import br.com.cortex.eleicoes.service.GeoJsonService;
import mil.nga.sf.geojson.FeatureCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import static java.util.Objects.nonNull;

@Component
public class GeoJsonServiceImpl implements GeoJsonService {

    private static final Logger logger = LoggerFactory.getLogger(GeoJsonServiceImpl.class);

    private static String API_BASE_URL = "https://servicodados.ibge.gov.br";

    //id de qquer tipo de localidade, na api do IBGE
    private static String API_LOCALIDADE_URL = "/api/v2/malhas/{id}";

    private static String GEOJSON_FORMAT = "application/vnd.geo+json";
    private static String GEOJSON_FORMAT_PARAM = "formato";


    @Autowired
    private WebClient.Builder webClientBean;


    @Override
    public FeatureCollection findByLocalidadeId(Integer id) throws ServiceException {

        try {
            FeatureCollection featureCollection = getWebClient(API_BASE_URL)
                    .get()
                    .uri(webClientBean -> webClientBean
                            .path(API_LOCALIDADE_URL)
                            .queryParam(GEOJSON_FORMAT_PARAM, GEOJSON_FORMAT)
                            .build(id))
                    .retrieve()
                    .bodyToMono(FeatureCollection.class)
                    .block();

            logger.info("Found geoJson info for id {}", id);

            if (nonNull(featureCollection)) {
                return featureCollection;
            }

        } catch (Exception e) {
            logger.error("GeoJsonServiceImpl -> findByLocalidadeId: ", e);
        }

        return null;
    }



    private WebClient getWebClient(String baseUrl) {

        return webClientBean
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

}
