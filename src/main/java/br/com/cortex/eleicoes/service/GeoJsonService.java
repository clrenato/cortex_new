package br.com.cortex.eleicoes.service;

import br.com.cortex.eleicoes.exception.ServiceException;
import mil.nga.sf.geojson.FeatureCollection;

public interface GeoJsonService {

    FeatureCollection findByLocalidadeId(Integer id) throws ServiceException;

}
