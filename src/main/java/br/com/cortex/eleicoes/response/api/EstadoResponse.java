package br.com.cortex.eleicoes.response.api;

import br.com.cortex.eleicoes.model.Estado;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "estado", "totalVotos", "candidatos" })
public class EstadoResponse extends BaseResponse {

    private Estado estado;

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
