package br.com.cortex.eleicoes.response.api;

import br.com.cortex.eleicoes.model.Regiao;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "regiao", "totalVotos", "candidatos" })
public class RegiaoResponse extends BaseResponse {

    private Regiao regiao;

    public Regiao getRegiao() {
        return regiao;
    }

    public void setRegiao(Regiao regiao) {
        this.regiao = regiao;
    }

}
