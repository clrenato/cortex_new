package br.com.cortex.eleicoes.response.api;

import br.com.cortex.eleicoes.model.Mesorregiao;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "mesorregiao", "totalVotos", "candidatos" })
public class MesorregiaoResponse extends BaseResponse {

    private Mesorregiao mesorregiao;

    public Mesorregiao getMesorregiao() {
        return mesorregiao;
    }

    public void setMesorregiao(Mesorregiao mesorregiao) {
        this.mesorregiao = mesorregiao;
    }
}
