package br.com.cortex.eleicoes.response.api;

import br.com.cortex.eleicoes.model.Partido;

public class PartidoResponse {

    private Partido partido;

    public Partido getPartido() {
        return partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }
}
