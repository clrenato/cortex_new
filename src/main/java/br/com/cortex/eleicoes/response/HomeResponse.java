package br.com.cortex.eleicoes.response;

import br.com.cortex.eleicoes.model.Partido;

import java.util.List;

public class HomeResponse {

    List<Partido> partidoList;

    public List<Partido> getPartidoList() {
        return partidoList;
    }

    public void setPartidoList(List<Partido> partidoList) {
        this.partidoList = partidoList;
    }
}
