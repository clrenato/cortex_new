package br.com.cortex.eleicoes.response.api;

import br.com.cortex.eleicoes.model.Microrregiao;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "microrregiao", "totalVotos", "candidatos" })
public class MicrorregiaoResponse extends BaseResponse {

    private Microrregiao microrregiao;

    public Microrregiao getMicrorregiao() {
        return microrregiao;
    }

    public void setMicrorregiao(Microrregiao microrregiao) {
        this.microrregiao = microrregiao;
    }
}
