package br.com.cortex.eleicoes.response.api;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.ArrayList;
import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BaseResponse {

    private Integer totalVotos = 0;
    private List<VotoResponse> candidatos = new ArrayList<>();

    public Integer getTotalVotos() {
        return totalVotos;
    }

    public void setTotalVotos(Integer totalVotos) {
        this.totalVotos = totalVotos;
    }

    public List<VotoResponse> getCandidatos() {
        return candidatos;
    }

    public void setCandidatos(List<VotoResponse> candidatos) {
        this.candidatos = candidatos;
    }
}
