package br.com.cortex.eleicoes.response.api;

import br.com.cortex.eleicoes.model.Municipio;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "municipio", "totalVotos", "candidatos" })
public class MunicipioResponse extends BaseResponse {

    private Municipio municipio;

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
}
