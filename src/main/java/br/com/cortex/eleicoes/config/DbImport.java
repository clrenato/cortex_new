package br.com.cortex.eleicoes.config;

import br.com.cortex.eleicoes.dao.*;
import br.com.cortex.eleicoes.model.*;
import br.com.cortex.eleicoes.utils.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.handler.codec.http.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Component
public class DbImport {

    private static final Logger logger = LoggerFactory.getLogger(DbImport.class);

    private static String API_BASE_LOCALIDADES_URL = "https://servicodados.ibge.gov.br/api/v1/localidades";
    private static String API_ESTADOS_PATH = "/estados";
    private static String API_MESO_PATH = "/estados/{estado_api_id}/mesorregioes";
    private static String API_MICRO_PATH = "/mesorregioes/{meso_id}/microrregioes";
    private static String API_MUNICIPIOS_PATH = "/microrregioes/{micro_id}/municipios";

    private final WebClient.Builder webClientBuilder = WebClient.builder();

    @Autowired
    private JdbcTemplate jdbcTemplateBean;

    @Autowired
    private UfDAO ufDAO;

    @Autowired
    private EstadoDAO estadoDAO;

    @Autowired
    private MunicipioDAO municipioDAO;

    @Autowired
    private CandidatoImportDAO candidatoImportDAO;

    @Autowired
    private CandidatoDAO candidatoDAO;

    @Autowired
    private PartidoDAO partidoDAO;

    @Autowired
    private MesorregiaoDAO mesorregiaoDAO;

    @Autowired
    private MicrorregiaoDAO microrregiaoDAO;

    @Autowired
    private RegiaoDAO regiaoDAO;


    //API imports
    private WebClient getWebClient(String baseUrl) {

        return webClientBuilder
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }



    private void setMesoAndMicroIds()  {

        try {
            List<Microrregiao> allMicro = microrregiaoDAO.findAll();

            for (Microrregiao m : allMicro) {

                int microId = m.getId();
                int mesoId = m.getMesorregiaoId();

                List<Municipio> municipios = municipioDAO.findByMicrorregiaoId(Arrays.asList(microId));

                for (Municipio mun : municipios) {

                    //imports por municipio
                    List<CandidatoImport> candidatoImports = candidatoImportDAO.findByMunicipioId(mun.getId());

                    //update micro e meso ids
                    for (CandidatoImport ci : candidatoImports) {

                        ci.setMicrorregiaoId(microId);
                        ci.setMesorregiaoId(mesoId);

                        candidatoImportDAO.updateMicroAndMesoIds(ci);

                        System.out.println("UPDATED: " + ci.toString());
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void setMicroIdForMunicipio() {

        try {
            List<Microrregiao> microList = microrregiaoDAO.findAll();

            for (Microrregiao micro : microList) {

                try {

                    List<Municipio> municipioApiList = getWebClient(API_BASE_LOCALIDADES_URL)
                            .get()
                            .uri(API_MUNICIPIOS_PATH, micro.getId())
                            .retrieve()
                            .bodyToFlux(Municipio.class)
                            .collectList()
                            .block();

                    for (Municipio municipioApi : municipioApiList) {

                        try {
                            Municipio municipio = municipioDAO.findById(municipioApi.getId());

                            if (nonNull(municipio)) {

                                if (nonNull(municipio.getMicrorregiaoId())) {
                                    System.out.println("next");
                                    continue;
                                }

                                municipio.setMicrorregiaoId(micro.getId());

                                municipioDAO.update(municipio);

                                System.out.println("UPDATED: " + municipio.toString());
                            }

                        } catch (Exception e) {
                            logger.error("ERROR findMunicipioById: ", e);
                        }

                    }

                } catch (Exception e) {
                    logger.error("ERROR callApi: ", e);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void setEstadoApiId() {

        List<Estado> estadoApiList = getWebClient(API_BASE_LOCALIDADES_URL)
        .get()
        .uri(API_ESTADOS_PATH)
        .retrieve()
        .bodyToFlux(Estado.class)
        .collectList()
        .block();

        for (Estado estadoApi : estadoApiList) {

            System.out.println( estadoApi.toString() );

            try {
                Estado estado = estadoDAO.findBySigla(estadoApi.getSigla());

                estado.setApiId(estadoApi.getId());

                estadoDAO.update(estado);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }


    private void importMeso() throws Exception {

        List<Estado> estados = estadoDAO.findAll();

        for (Estado estado : estados) {

            List<Mesorregiao> mesoList = getWebClient(API_BASE_LOCALIDADES_URL)
                    .get()
                    .uri(API_MESO_PATH, estado.getApiId())
                    .retrieve()
                    .bodyToFlux(Mesorregiao.class)
                    .collectList()
                    .block();

            for(Mesorregiao meso : mesoList) {

                meso.setEstadoId(estado.getId());

                mesorregiaoDAO.save(meso);

                System.out.println("SAVED " + meso.toString());
            }

        }
    }



    private void importMicro() {

        try {

            List<Mesorregiao> mesoList = mesorregiaoDAO.findAll();

            for (Mesorregiao meso : mesoList) {

                List<Microrregiao> microList = getWebClient(API_BASE_LOCALIDADES_URL)
                        .get()
                        .uri(API_MICRO_PATH, meso.getId())
                        .retrieve()
                        .bodyToFlux(Microrregiao.class)
                        .collectList()
                        .block();

                for (Microrregiao micro : microList) {

                    micro.setMesorregiaoId(meso.getId());

                    microrregiaoDAO.save(micro);

                    System.out.println("SAVED " + micro.toString());

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    private Estado parseEstadoJson(List<Object> list) {

        try {
            Estado estado = new Estado();
            estado.setUfCode((String)list.get(0));
            estado.setSigla((String)list.get(1));
            estado.setNome((String)list.get(2));
            estado.setCode((String)list.get(3));

            Uf uf = ufDAO.findBySigla(estado.getSigla());
            estado.setUfId(uf.getId());

            return estado;

        } catch (Exception ex) {
            System.out.println("parseEstado exception" + ex.getLocalizedMessage());
        }

        return null;
    }

    private Municipio parseMunicipioJson(List<Object> list) {

        try {
            Municipio municipio = new Municipio();
            municipio.setNome((String) list.get(0));
            municipio.setId(Integer.parseInt((String) list.get(1)));
            municipio.setMuCode((String) list.get(2));
            municipio.setSkuCode((String) list.get(3));
            municipio.setUfName((String) list.get(4));
            municipio.setUf((String) list.get(5));

            Estado estado = estadoDAO.findBySigla(municipio.getUf());
            municipio.setEstadoId(estado.getId());

            return municipio;

        } catch (Exception ex) {
            System.out.println("parseMunicipio exception" + ex.getLocalizedMessage());
        }

        return null;
    }


    private CandidatoImport parseCandidatoJson(List<Object> list) {

        try {
            CandidatoImport candidatoImport = new CandidatoImport();
            candidatoImport.setPartido((String)list.get(0));
            candidatoImport.setNome((String)list.get(1));
            candidatoImport.setVotos((Integer)list.get(2));
            candidatoImport.setDoubleValue((Double)list.get(3));
            candidatoImport.setLetter((String)list.get(4));

            String seoNome = StringUtils.toSlug(candidatoImport.getNome() + " " + candidatoImport.getPartido());
            candidatoImport.setSeoNome(seoNome);

            return candidatoImport;

        } catch (Exception ex) {
            System.out.println("parseCandidato exception" + ex.getLocalizedMessage());
        }

        return null;
    }



    public void setCandidatoImportIds() {

        try {
            List<Candidato> allCandidatos = candidatoDAO.findAll();

            for (Candidato candidato : allCandidatos) {

                List<CandidatoImport> imports = candidatoImportDAO.findByName(candidato.getNome());

                if (nonNull(imports)) {

                    for (CandidatoImport ci : imports) {

                        ci.setCandidatoId(candidato.getId());
                        ci.setPartidoId(candidato.getPartidoId());

                        candidatoImportDAO.update(ci);

                        System.out.println("UPDATED: " + candidato.getId() + " / " + ci.toString());
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public void importJsonToMysql() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();

            File file = ResourceUtils.getFile("classpath:import.json");
            InputStream in = new FileInputStream(file);

            List<?> list = objectMapper.readValue(in, List.class);

            for (Object obj : list) {

                int estadoId = 0;
                int municipioId = 0;

                //UF ou municipio
                List<Object> rootObject = (ArrayList)obj;

                if (isNull(rootObject) || rootObject.isEmpty()) {
                    continue;
                }

                String check = (String)rootObject.get(0);

                if (check.equalsIgnoreCase("UF")) {//estado

                    Estado estadoJson = parseEstadoJson(rootObject);

                    if (isNull(estadoJson)) {
                        continue;
                    }

                    Estado estado = estadoDAO.findBySigla(estadoJson.getSigla());

                    estadoId = estado.getId();
////
////                    if (nonNull(estado)) {
////                        //System.out.println("estado: " + estado.toString());
////                        //estadoDAO.save(estado);
////
////                    } else {
////                        continue;
////                    }

                } else {//municipio

                    Municipio municipioJson = parseMunicipioJson(rootObject);

                    if (isNull(municipioJson)) {
                        continue;
                    }

                    Municipio municipio = municipioDAO.findById(municipioJson.getId());

                    municipioId = municipio.getId();

//                    if (nonNull(municipio)) {
//                        System.out.println("municipio: " + municipio.toString());
//
//                        municipioId = municipioDAO.save(municipio);
//
//                    } else {
//                        continue;
//                    }

                }

                //indice do primeiro node de candidato
                int idxList = estadoId > 0 ? 4 : (municipioId > 0 ? 6 : 0);

                //sanity
                if (idxList == 0) {
                    continue;
                }

                for (int i = idxList, j = rootObject.size(); i < j; i++) {

                    List<Object> childObject = (ArrayList) rootObject.get(i);

                    CandidatoImport candidatoImport = parseCandidatoJson(childObject);

                    if (nonNull(candidatoImport)) {

                        //test estado ou munic.
                        if (estadoId > 0) {
                            candidatoImport.setEstadoId(estadoId);

                        } else if (municipioId > 0) {
                            candidatoImport.setMunicipioId(municipioId);
                        }

                        candidatoImportDAO.save(candidatoImport);

                        System.out.println("SAVED candidato: " + candidatoImport.toString());
                    }

                }

            }

        } catch(Exception e) {
            e.printStackTrace();
        }

    }

}
