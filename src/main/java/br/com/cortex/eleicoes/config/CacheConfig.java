package br.com.cortex.eleicoes.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.crypto.KeyGenerator;

@Configuration
@EnableCaching
public class CacheConfig {

    @Value("${spring.cache.geojson}")
    private String geoJsonCache;

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(geoJsonCache);
    }

}
