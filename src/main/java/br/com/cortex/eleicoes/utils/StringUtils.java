package br.com.cortex.eleicoes.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.Normalizer;
import java.util.Locale;
import java.util.regex.Pattern;

public class StringUtils {

    private static final Logger logger = LoggerFactory.getLogger(StringUtils.class);

    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");
    private static final Pattern EDGESDHASHES = Pattern.compile("(^-|-$)");


    public static String toSlug(String input) {

        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        slug = EDGESDHASHES.matcher(slug).replaceAll("");

        //sanity
        if(slug != null && slug.contains("--")) {
            slug = slug.replace("--", "-");
        }

        return slug.toLowerCase(Locale.ENGLISH);
    }


    public static String normalize(String str) {

        try {
            return str.trim().replaceAll("\\s{1,}", "-");

        } catch(Exception e) {
            logger.error("StringUtils->normalize: ", e);
        }

        return str;
    }


    public static String normalizeToUrl(String str) {

        try {
            return normalize(str).toLowerCase();

        } catch(Exception e) {
            logger.error("PriceUtils->normalizeToUrl: ", e);
        }

        return str;
    }

}
