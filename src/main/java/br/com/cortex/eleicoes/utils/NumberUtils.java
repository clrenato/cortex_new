package br.com.cortex.eleicoes.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberUtils {

    private static final Logger logger = LoggerFactory.getLogger(NumberUtils.class);

    public static boolean isNumeric(String s) {

        try {
            int number = Integer.parseInt(s);
            return true;

        } catch (NumberFormatException e) {
        }

        return false;
    }


}
