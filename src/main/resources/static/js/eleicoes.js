//global map
var map = null;
var basemap = null;

let select_list = [
    ['select_regiao', '/api/eleicao/2014/presidente/primeiro-turno/partidos/{partido}'], //regioes
    ['select_estado', '/api/eleicao/2014/presidente/primeiro-turno/partidos/{partido}/{uf}'], //estados
    ['select_meso', '/api/eleicao/2014/presidente/primeiro-turno/partidos/{partido}/{uf}/{reg}'], //mesorregioes
    ['select_micro', '/api/eleicao/2014/presidente/primeiro-turno/partidos/{partido}/{uf}/{reg}/{meso}'], //microrregioes
    ['select_municipio', '/api/eleicao/2014/presidente/primeiro-turno/partidos/{partido}/{uf}/{reg}/{meso}/{micro}'] //municipios
];


function getAjaxResponse(type, partido, uf = '', reg = '', meso = '', micro = '') {

    let _url = select_list[type][1]
            .replace("{partido}", partido)
            .replace("{uf}", uf)
            .replace("{reg}", reg)
            .replace("{meso}", meso)
            .replace("{micro}", micro);

    $.getJSON(_url, function(result){

        $(`#${select_list[type][0]}`).empty();

        switch(type) {

            case 0: //regioes
                $.each(result, function(i, field){
                    $(`#${select_list[type][0]}`)
                        .append(`<option value='${field.regiao.id}' data-votos='${field.total_votos}'>${field.regiao.nome} (${field.regiao.sigla})</option>`);
                });

                break;

            case 1: //estados
                $.each(result, function(i, field){
                    $(`#${select_list[type][0]}`)
                        .append(`<option value='${field.estado.id}' data-votos='${field.total_votos}'>${field.estado.nome} (${field.estado.sigla})</option>`);
                });

                break;

            case 2: //mesos
                $.each(result, function(i, field){
                    $(`#${select_list[type][0]}`)
                        .append(`<option value='${field.mesorregiao.id}' data-votos='${field.total_votos}'>${field.mesorregiao.nome} (${field.mesorregiao.estado.sigla})</option>`);
                });

                break;

            case 3: //micros
                $.each(result, function(i, field){
                    $(`#${select_list[type][0]}`)
                        .append(`<option value='${field.microrregiao.id}' data-votos='${field.total_votos}'>${field.microrregiao.nome} (${field.microrregiao.mesorregiao.estado.sigla})</option>`);
                });

                break;

            case 4: //municipios
                $.each(result, function(i, field){
                    $(`#${select_list[type][0]}`)
                        .append(`<option value='${field.municipio.id}' data-votos='${field.total_votos}'>${field.municipio.nome} (${field.municipio.microrregiao.mesorregiao.estado.sigla})</option>`);
                });

                break;


            default:
        }

        $(`#${select_list[type][0]}`).parent().show();


    });
}

/*
 * ordena DESC pelo numero de votos,
    calcula um decrescimo na opacidade
    e retorna opacidade pro total de votos especificado
 */
function getOpacity(optArray, _votosId) {

    optArray.sort(function(a, b) {return b - a});

    var _op = 1.0,
        _portion = _op/optArray.length;

    for (var i = 0; i < optArray.length; i++) {

        if (i > 0) {
            _op -= _portion;

            if (optArray[i] == Number(_votosId)) {

                return Math.round(_op * 10) / 10;
            }
        }
    }

    return 1.0;
}


function getGeoJson(localidade_id, type) {

    //select correspondente
    let $select = $(`#${select_list[type][0]}`);

    //exibe 'loading'
    $select.parent().find('img').show();

    //option clicada
    let $option = $select.find(`option[value = ${localidade_id}]`);

    //votos da option clicada
    let $optVotos = $option.attr('data-votos');

    var optSelected = [];
    $select.children('option:selected').each( function() {
        var $this = $(this);
        optSelected.push( Number($this.attr('data-votos')) );
    });

    //opacidade da layer da opção escolhida
    var _optOpacity = getOpacity(optSelected, $optVotos);

    //recupera geoJson da option (localidade) selecionada
    try {
        let _url = `/api/eleicao/2014/presidente/primeiro-turno/geojson/${localidade_id}?type=${type}`;

        $.getJSON(_url, function(result){

            //inclui no mapa, setando opacidade
            var dataLayer = L.geoJson(result, {
                onEachFeature: function(feature, layer) {
                    layer.setStyle({fillColor: '#000000'});

                    layer.bindPopup( $option.text() + "<br/>Total de votos: " + $optVotos );

                    layer.on('mouseover',function(ev) {
                        layer.openPopup();
                    });
                },
                style: {
                  color: '#000000',
                  fillColor: '#000000',
                  weight: 0,
                  opacity: 1,
                  fillOpacity: _optOpacity
                }
            });

            dataLayer.addTo(map);

            //oculta 'loading'
            $select.parent().find('img').hide();

        });

    } catch(err) {
      console.log(`Ajax error: ${err}`);
    }

}


function layerStyle(feature) {
    return {
       color: '#000000',
       fillColor: '#000000',
       weight: 1,
       opacity: 1,
       fillOpacity: 1
    };
}



function loadOptions(type) {

    //zera selects subsequentes
    for (var i = type; i < select_list.length; i++) {

        $(`#${select_list[i][0]}`).empty();
        $(`#${select_list[i][0]}`).parent().hide();
    }

    //limpa o mapa
    reloadMap();

    //refaz a busca
    let _partido = $('#select_partido').val() || '',
        _regioes = $('#select_regiao').val().join('|') || '',
        _estados = $('#select_estado').val().join('|') || '',
        _mesos = $('#select_meso').val().join('|')  || '',
        _micros = $('#select_micro').val().join('|')  || '',
        _munics = $('#select_municipio').val().join('|')  || '';

    //recupera geojson de cada select->option selecionado
    var _sel = null;
    var _type = null;

    if (_munics != '') {
        _sel = $('#select_municipio').val();
        _type = 4;

    } else if (_micros != '') {
        _sel = $('#select_micro').val();
        _type = 3;

    } else if (_mesos != '') {
        _sel = $('#select_meso').val();
        _type = 2;

    } else if (_estados != '') {
        _sel = $('#select_estado').val();
        _type = 1;

    } else if (_regioes != '') {
        _sel = $('#select_regiao').val();
        _type = 0;
    }

    if (_sel != null && _type != null) {

        //reseta o mapa e recarrega layers solicitadas
        reloadMap();

       _sel.forEach(function(opt) {
           getGeoJson(opt, _type);
       });
    }

    //carrega selects até minucipio apenas
    if (_type < 4) {
        getAjaxResponse(type, _partido, _regioes, _estados, _mesos, _micros);
    }

}


function initMap() {

    map = L.map(document.getElementById('map'), {
            center: [-15.0, -54.0],
            zoom: 4
    });

    basemap = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {});
    basemap.addTo(map);
}


function reloadMap() {

    if (map) {
        map.eachLayer((layer) => {

          //preserva basemap
          if (layer && (layer._url == undefined || layer._url.indexOf('tile.osm.org') == -1)) {
            layer.remove();
          }
        });
    }
}


$( document ).ready(function() {

    initMap();

    $('#select_list .wrapper_select select').each(function(i) {

        $(this).change(function() {

            if ( $(this).val() ) {
                loadOptions(i);
            }
        });

    });

});